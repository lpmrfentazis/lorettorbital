from lorettOrbital.scheduler import Scheduler, SchedulerConfig, sources
from datetime import datetime

stationName = "test"

lat = 55.79713
lon = 37.57931
alt = 0.16 # in km

horizon = 50
minApogee = 55

# select tle group
source = sources["weather"]
# IDK, meteor-m2 2 disappeared from active tle on celestrack

path: str = '.'


if __name__ == "__main__":
    
    # create Config-object
    config = SchedulerConfig(stationName=stationName, 
                             lat=lat, 
                             lon=lon,
                             alt=alt,
                             horizon=horizon,
                             minApogee=minApogee,
                             source=source,
                             path=path)
    
    # You can use youre logger like verboseLogger
    logger = None
    
    # create Scheduler-object
    # The first run may take some time. 
    # The tle is being loaded
    scheduler = Scheduler(config=config, logger=logger)
    
    # You can check the tle update manually
    # If tle < 3 days there will be no update
    scheduler.update()
    
    now = datetime.utcnow()
    
    # return list of SatPass-objects
    schedule = scheduler.getSchedule(timeStart=now, length=24)
    
    # select nearest pass 
    satPass = schedule[0]
    
    # get the track. 
    #   timestep - every how many seconds you need to calculate a new point
    #   save - if true - create track-file in <workPath> / tracks
    # Return the list of SphereCoordinates
    track = scheduler.getSateliteTrack(satPass=satPass, timestep=0.5, save=True)
    
    print('azimuth', "elevation", "time", sep="\t")
    for i in track:
        print(i.azimuth, i.elevation, i.time, sep="\t")
    

