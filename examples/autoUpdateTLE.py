from lorettOrbital.scheduler import Scheduler, SchedulerConfig, sources
from time import sleep

# python3 -m pip install schedule
# https://schedule.readthedocs.io/en/stable/
import schedule as sch

stationName = "test"

lat = 55.79713
lon = 37.57931
alt = 0.16 # in km

horizon = 50
minApogee = 55

# select tle group
source = sources["weather"]
# IDK, meteor-m2 2 disappeared from active tle on celestrack

path: str = '.'


if __name__ == "__main__":
    
    # create Config-object
    config = SchedulerConfig(stationName=stationName, 
                             lat=lat, 
                             lon=lon,
                             alt=alt,
                             horizon=horizon,
                             minApogee=minApogee,
                             source=source,
                             path=path)
    
    # You can use youre logger like verboseLogger
    logger = None
    
    # create Scheduler-object
    # The first run may take some time. 
    # The tle is being loaded
    scheduler = Scheduler(config=config, logger=logger)
    
    # You can check the tle update manually
    # If tle < 3 days there will be no update
    scheduler.update()
    
    # do tle check automaticaly
    sch.every(6).hours.do(scheduler.update)
    
    sleep(60 * 60 * 6)
    

